package ch.asyncsystems

import ch.asyncsystems.repo.Customer
import ch.asyncsystems.repo.CustomerRepositoy
import ch.asyncsystems.repo.CustomerType
import ch.asyncsystems.repo.TaskRepository
import org.json.JSONObject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/customers")
class CustomerResource {

    /**
     * @return Returns all customers
     * Returns a list of all the customers.
     */
    @GET
    fun getCustomers(): List<Customer> {
        return CustomerRepositoy.list()
    }

    /**
     * @param jsonString: The jsonString, with the needed data of the new customer.
     * Generates a new customer, with the data, passed by the jsonString parameter.
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    fun addCustomer(jsonString: String) {

        val jsonObject = JSONObject(jsonString)

        val customer: Customer = if(CustomerType.PRIVATE.type.equals(jsonObject["type"].toString(), true)) {
            if(jsonObject["handy"].toString().isEmpty()) {
                Customer(customerType = CustomerType.PRIVATE, salut = jsonObject["salut"].toString(), name = jsonObject["name"].toString(), email = jsonObject["email"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
            } else {
                Customer(customerType = CustomerType.PRIVATE, salut = jsonObject["salut"].toString(), name = jsonObject["name"].toString(), handy = jsonObject["handy"].toString(), email = jsonObject["email"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
            }
        } else {
            if(jsonObject["salut"].toString().isEmpty() || jsonObject["name"].toString().isEmpty()) {
                if(jsonObject["handy"].toString().isEmpty()) {
                    Customer(customerType = CustomerType.COMPANY, companyName = jsonObject["companyName"].toString(), email = jsonObject["email"].toString(), legalForm = jsonObject["legalForm"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
                } else {
                    Customer(customerType = CustomerType.COMPANY, companyName = jsonObject["companyName"].toString(), handy = jsonObject["handy"].toString(), email = jsonObject["email"].toString(), legalForm = jsonObject["legalForm"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
                }
            } else {
                if(jsonObject["handy"].toString().isEmpty()) {
                    Customer(customerType = CustomerType.COMPANY, salut = jsonObject["salut"].toString(), name = jsonObject["name"].toString(), companyName = jsonObject["companyName"].toString(), email = jsonObject["email"].toString(),  legalForm = jsonObject["legalForm"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
                } else {
                    Customer(customerType = CustomerType.COMPANY, salut = jsonObject["salut"].toString(), name = jsonObject["name"].toString(), companyName = jsonObject["companyName"].toString(), handy = jsonObject["handy"].toString(), email = jsonObject["email"].toString(), legalForm = jsonObject["legalForm"].toString(), street = jsonObject["street"].toString(), houseNo = jsonObject["houseNo"].toString(), postalCode = jsonObject["postalCode"].toString(), town = jsonObject["town"].toString(), countryCode = jsonObject["countryCode"].toString())
                }
            }
        }
        CustomerRepositoy.add(customer)
    }

    /**
     * @param _id: Form-Paramter, passed by a HTMLForm
     * Deletes the paramter-specified customer from the database.
     */
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    fun deleteCustomer(@FormParam("_id") _id: String) {
        TaskRepository.filteredDelete("{customerID: '${_id}'}")
        val customer = CustomerRepositoy.single(_id)
        CustomerRepositoy.delete(customer)
    }

}