package ch.asyncsystems.repo

import com.mongodb.client.MongoDatabase
import ch.asyncsystems.util.Configuration
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection

data class Bill(val _id: String? = null,
val billID: Long,
val customerID: String?)

class BillRepository {
    companion object {

        val client = KMongo.createClient("mongodb://${Configuration.getUsername()}:${Configuration.getPassword()}@${Configuration.getHost()}:${Configuration.getPort()}")
        val database: MongoDatabase = client.getDatabase("asynctime")
        val col = database.getCollection<Bill>()

        fun add(bill: Bill): Boolean {
            val res = col.insertOne(bill)
            return res.wasAcknowledged()
        }

        fun count(): Long {
            return col.countDocuments()
        }
    }
}