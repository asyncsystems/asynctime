package ch.asyncsystems.repo

import com.mongodb.BasicDBObject
import com.mongodb.client.MongoDatabase
import ch.asyncsystems.util.Configuration
import org.litote.kmongo.*

data class Task(val _id: String? = null,
                val taskType: TaskType,
                val customerID: String,
                val executionDate: String? = null,
                val description: String,
                val singlePrice: Double,
                val quantity: Double? = null,
                val timeStart: String? = null,
                val timeEnd: String? = null,
                val contractStart: String? = null,
                val contractEnd: String? = null,
                val vat: Double,
                val total: Double? = null,
                val booked: Boolean = false)

enum class TaskType(val type: String) {
    SINGLE("single"),
    TIMED("timed"),
    CONTRACT("contract")
}

class TaskRepository {
    companion object {

        val client = KMongo.createClient("mongodb://${Configuration.getUsername()}:${Configuration.getPassword()}@${Configuration.getHost()}:${Configuration.getPort()}")
        val database: MongoDatabase = client.getDatabase("asynctime")
        val col = database.getCollection<Task>()

        fun list(): List<Task> {
            val list = arrayListOf<Task>()
            col.find().iterator().forEach { data ->
                list.add(data)
            }

            return list
        }

        fun listFilter(filter: String): List<Task> {
            val list = arrayListOf<Task>()
            col.find(filter).iterator().forEach { data ->
                list.add(data)
            }
            return list
        }

        fun add(task: Task): Boolean {
            val res = col.insertOne(task)
            return res.wasAcknowledged()
        }

        fun update(task: Task): Boolean {
            val res = col.updateOneById(task._id!!, task)
            return res.wasAcknowledged()
        }

        fun delete(task: Task): Boolean {
            val res = col.deleteOneById(task._id!!)
            return res.wasAcknowledged()
        }

        fun filteredDelete(filter: String): Boolean {
            val res = col.deleteMany(filter)
            return res.wasAcknowledged()
        }

        fun getById(id: String): Task? {
            val res = col.findOneById(id) ?: return null
            return res
        }
    }
}
