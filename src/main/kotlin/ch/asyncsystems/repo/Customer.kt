package ch.asyncsystems.repo

import com.mongodb.client.MongoDatabase
import ch.asyncsystems.util.Configuration
import org.litote.kmongo.KMongo
import org.litote.kmongo.deleteOneById
import org.litote.kmongo.find
import org.litote.kmongo.getCollection

data class Customer(val _id: String? = null,
                    val customerType: CustomerType,
                    val salut: String? = null, // Herr, Frau, (Divers?)
                    val name: String? = null,
                    val handy: String? = null,
                    val email: String,
                    val companyName: String? = null,
                    val legalForm: String? = null, // Einzelfirma, GmbH, AG, & Co, Verein, Zweigniederlassung, Stiftung, Genossenschaft, Sonstige)
                    val street: String,
                    val houseNo: String,
                    val postalCode: String,
                    val town: String,
                    val countryCode: String)

enum class CustomerType(val type: String) {
    PRIVATE("private"),
    COMPANY("company")
}

class CustomerRepositoy {
    companion object {

        val client = KMongo.createClient("mongodb://${Configuration.getUsername()}:${Configuration.getPassword()}@${Configuration.getHost()}:${Configuration.getPort()}")
        val database: MongoDatabase = client.getDatabase("asynctime")
        val col = database.getCollection<Customer>()

        fun list(): List<Customer> {
            val list = arrayListOf<Customer>()
            col.find().iterator().forEach { dat ->
                list.add(dat)
            }

            return list
        }

        fun single(id : String): Customer {
            return col.find("{ _id: '$id' }").single()
        }

        fun add(customer: Customer): Boolean {
            val res = col.insertOne(customer)
            return res.wasAcknowledged()
        }

        fun delete(customer: Customer): Boolean {
            val res = col.deleteOneById(customer._id!!)
            return res.wasAcknowledged()
        }
    }
}
