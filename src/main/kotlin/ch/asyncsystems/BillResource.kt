package ch.asyncsystems

import be.quodlibet.boxable.BaseTable
import be.quodlibet.boxable.Cell
import be.quodlibet.boxable.Row
import be.quodlibet.boxable.line.LineStyle
import ch.asyncsystems.repo.BillRepository
import ch.asyncsystems.repo.CustomerRepositoy
import ch.asyncsystems.repo.Task
import ch.asyncsystems.repo.TaskRepository
import ch.asyncsystems.util.Configuration
import ch.asyncsystems.util.FileResourcesUtils
import ch.asyncsystems.util.Time
import ch.asyncsystems.util.difference
import net.codecrete.qrbill.canvas.PDFCanvas
import net.codecrete.qrbill.generator.*
import org.apache.commons.io.FileUtils
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory
import org.json.JSONArray
import java.awt.Color
import java.io.File
import java.text.SimpleDateFormat
import javax.imageio.ImageIO
import javax.ws.rs.*
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import kotlin.math.ceil

@Path("/bills")
class BillResource {

    /**
     * @return Returns all tasks, that have not yet been put into a bill.
     * Send all tasks, that have not yet put into a bill.
     */
    @GET
    fun getUnbookedTasks(): List<Task> {
        return TaskRepository.listFilter("{booked: false}")
    }

    /**
     * @param customerID: Includes the id of the specified customer.
     * @return returns all the tasks, that have not yet been put into a bill.
     * Send all tasks of a customer back to the UI, that weren't put inside a bill yet.
     */
    @POST
    @Path("/unbooked")
    @Consumes(MediaType.TEXT_PLAIN)
    fun searchUnbookedTasks(customerID: String): List<Task> {
        return TaskRepository.listFilter("{booked: false, customerID: '${customerID}'}")
    }

    /**
     * @param task: includes a jsonarray-string, that gets converted to a jsonarray, to access all the data.
     * @return Returns the filename of the generated bill, in form fo a json-object.
     * Generates a PDF-Bill, with the checked tasks and stores it inside the container, as a cache.
     */
    @POST
    @Path("/generate")
    @Consumes(MediaType.TEXT_PLAIN)
    fun generateBill(task: String): String {
        val jsonarray = JSONArray(task)

        var totalprice = 0.0
        var id = ""
        for (i in 0 until jsonarray.length()) {
            println(jsonarray.getJSONObject(i)["total"])
            totalprice += roundOff(jsonarray.getJSONObject(i)["total"].toString().toDouble())
            id = jsonarray.getJSONObject(i)["customerID"].toString()
        }

        totalprice = roundOff(totalprice)

        val cmToInch = 0.3937007874
        val pointsPerInch = 27.0

        val bill = Bill()
        bill.account = Configuration.getAccount()
        bill.setAmountFromDouble(totalprice)
        bill.currency = Configuration.getCurrency()

        val creditor = Address()
        creditor.name = Configuration.getCreditorName()
        creditor.street = Configuration.getCreditorStreet()
        creditor.houseNo = Configuration.getCreditorHouseNo()
        creditor.postalCode = Configuration.getCreditorPostalCode()
        creditor.town = Configuration.getCreditorTown()
        creditor.countryCode = Configuration.getCreditorCountryCode()
        bill.creditor = creditor

        val customer = CustomerRepositoy.single(id)
        val debtor = Address()
        debtor.name = customer.name
        debtor.street = customer.street
        debtor.houseNo = customer.houseNo
        debtor.postalCode = customer.postalCode
        debtor.town = customer.town
        debtor.countryCode = customer.countryCode
        bill.debtor = debtor

        val format = bill.format
        format.graphicsFormat = GraphicsFormat.PDF
        format.outputSize = OutputSize.A4_PORTRAIT_SHEET
        format.language = Language.DE
        bill.format = format

        val internaBill: ch.asyncsystems.repo.Bill =
            ch.asyncsystems.repo.Bill(billID = BillRepository.count(), customerID = customer._id)

        val pdf = QRBill.generate(bill)
        val canvas = PDFCanvas(pdf, 0)

        val letterAddress = arrayOf(
            customer.salut,
            debtor.name,
            "${debtor.street} ${debtor.houseNo}",
            "${debtor.postalCode} ${debtor.town}"
        )
        canvas.putTextLines(
            letterAddress, /* Width */
            ((3.0 * cmToInch) * pointsPerInch), /* Height */
            ((22.9 * cmToInch) * pointsPerInch),
            12,
            0.0
        )

        val simpleDateFormat = SimpleDateFormat("yyyyMMdd")

        val titleText = arrayOf("Invoice No. ${internaBill.billID}")
        canvas.putTextLines(
            titleText,
            /* Width */ ((2.0 * cmToInch) * pointsPerInch),
            /* Height */ ((19.0 * cmToInch) * pointsPerInch),
            13,
            0.0
        )

        val doc = PDDocument.load(canvas.toByteArray())
        val page = doc.getPage(0)
        page.cropBox = PDRectangle.A4

        val stream = PDPageContentStream(doc, page, PDPageContentStream.AppendMode.PREPEND, false)

        val app = FileResourcesUtils()

        val image = LosslessFactory.createFromImage(doc, ImageIO.read(app.getFileFromResourceAsStream("img/asyncsystems.png")))
        stream.drawImage(image,
            ((38.7*cmToInch)*pointsPerInch).toFloat(),
            ((72.3*cmToInch)*pointsPerInch).toFloat()
        )

        val margin = 50f
        val yStartNewPage = page.mediaBox.height - (2 * margin)
        val tablewidth = page.mediaBox.width - (2 * margin)

        val drawContent = true
        val bottomMargin = 70f
        val yPosition = 565f

        val table = BaseTable(yPosition, yStartNewPage, bottomMargin, tablewidth, margin, doc, page, true, drawContent)

        val headerRow: Row<PDPage> = table.createRow(15f)

        var cell: Cell<PDPage> = headerRow.createCell(25f, "Datum/Laufzeit")
        cell.setLeftBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setRightBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setTopBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.fillColor = Color.LIGHT_GRAY

        cell = headerRow.createCell(39f, "Beschreibung")
        cell.setLeftBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setRightBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setTopBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.fillColor = Color.LIGHT_GRAY

        cell = headerRow.createCell(13f, "Netto")
        cell.setLeftBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setRightBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setTopBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.fillColor = Color.LIGHT_GRAY

        cell = headerRow.createCell(10f, "MwSt.")
        cell.setLeftBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setRightBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setTopBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.fillColor = Color.LIGHT_GRAY

        cell = headerRow.createCell(13f, "Total")
        cell.setLeftBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setRightBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.setTopBorderStyle(LineStyle(Color.BLACK, 0f))
        cell.fillColor = Color.LIGHT_GRAY
        table.addHeaderRow(headerRow)

        val swissDate = SimpleDateFormat("dd.MM.yyyy")

        for (i in 0 until jsonarray.length()) {
            val row: Row<PDPage> = table.createRow(12f)
            if (jsonarray.getJSONObject(i)["taskType"].toString() != "CONTRACT") {
                cell = row.createCell(25f, jsonarray.getJSONObject(i)["executionDate"].toString())
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            } else {
                cell = row.createCell(
                    25f,
                    "${jsonarray.getJSONObject(i)["contractStart"]} - ${jsonarray.getJSONObject(i)["contractEnd"]}"
                )
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            }
            if (jsonarray.getJSONObject(i)["taskType"].toString() != "TIMED") {
                cell = if(jsonarray.getJSONObject(i)["taskType"].toString() == "SINGLE") {
                    row.createCell(39f, jsonarray.getJSONObject(i)["description"].toString() + " | Anzahl: ${jsonarray.getJSONObject(i)["quantity"]}")
                } else {
                    row.createCell(39f, jsonarray.getJSONObject(i)["description"].toString())
                }
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            } else {
                val timeDiff = difference(
                    Time(
                        Integer.parseInt(jsonarray.getJSONObject(i)["timeStart"].toString().split(":")[0]),
                        Integer.parseInt(jsonarray.getJSONObject(i)["timeStart"].toString().split(":")[1])
                    ),
                    Time(
                        Integer.parseInt(jsonarray.getJSONObject(i)["timeEnd"].toString().split(":")[0]),
                        Integer.parseInt(jsonarray.getJSONObject(i)["timeEnd"].toString().split(":")[1])
                    )
                )
                cell = row.createCell(
                    39f,
                    "${jsonarray.getJSONObject(i)["description"]} | Dauer: ${timeDiff.hours}:${timeDiff.minutes}".replace(
                        "-",
                        ""
                    ) + " h"
                )
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            }
            if (jsonarray.getJSONObject(i)["taskType"].toString() != "TIMED") {
                cell = row.createCell(13f, " ${bill.currency} ${jsonarray.getJSONObject(i)["singlePrice"].toString()}")
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            } else {
                cell =
                    row.createCell(13f, " ${bill.currency} ${jsonarray.getJSONObject(i)["singlePrice"].toString()} / h")
                cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
                cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            }
            cell = row.createCell(10f, "${jsonarray.getJSONObject(i)["vat"].toString()} %")
            cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
            cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
            cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
            cell = row.createCell(13f, "${bill.currency} ${jsonarray.getJSONObject(i)["total"].toString()}")
            cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
            cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
            cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))

            val findTask: Task? = TaskRepository.getById(jsonarray.getJSONObject(i)["_id"].toString())
            if (findTask != null) {
                val bookedTask = Task(
                    _id = findTask._id,
                    taskType = findTask.taskType,
                    customerID = findTask.customerID,
                    executionDate = findTask.executionDate,
                    description = findTask.description,
                    singlePrice = findTask.singlePrice,
                    quantity = findTask.quantity,
                    timeStart = findTask.timeStart,
                    timeEnd = findTask.timeEnd,
                    contractStart = findTask.contractStart,
                    contractEnd = findTask.contractEnd,
                    vat = findTask.vat,
                    total = findTask.total,
                    booked = true
                )
                TaskRepository.update(bookedTask)
            }
        }

        val row: Row<PDPage> = table.createRow(12f)
        cell = row.createCell(87f, "Total zahlbar innert 30 Tagen")
        cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setBottomBorderStyle(LineStyle(Color.WHITE, 0f))
        cell = row.createCell(13f, "${bill.currency} $totalprice")
        cell.setLeftBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setRightBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setTopBorderStyle(LineStyle(Color.WHITE, 0f))
        cell.setBottomBorderStyle(LineStyle(Color.WHITE, 0f))

        table.draw()
        stream.close()

        BillRepository.add(internaBill)

        val filename: String = "Invoice_${BillRepository.count()}-${debtor.name.replace(" ", "_")}"

        doc.save(filename)
        doc.close()

        return """{"filename":"$filename"}"""
    }

    /**
     * @param filename: Filename of the bill, that's going to be downloaded.
     * @return Returns the File
     * Downloads the PDF-bill, with the specified filename-parameter.
     */
    @GET
    @Path("/download/{filename}")
    fun downloadBill(@PathParam("filename") filename: String): Response? {
        val file = File(filename)
        if(file.exists()) {
            val response = Response.ok(FileUtils.readFileToByteArray(file))
            response.type(MediaType.APPLICATION_OCTET_STREAM)
            response.header(HttpHeaders.CONTENT_LENGTH, file.length())
            response.header(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=\"${file.name}.pdf\""))
            return response.build()
        }
        return null
    }

    /**
     * @param value: The value, that need to be rounded to the nearest x.x5.
     * @return Returns the nearest x.x5 value.
     * Rounds the passed parameter to the nearest 0.05 value, for currencies.
     *
     * e.g.
     * 12.33 --> 12.35,
     * 12.36 --> 12.35,
     * 12.38 --> 12.40
     */
    fun roundOff(value: Double): Double {
        return ceil(value / 0.05) * 0.05
    }
}