package ch.asyncsystems.util

import java.io.File
import java.io.InputStream
import java.net.URL

class FileResourcesUtils {

    fun getFileFromResourceAsStream(fileName: String): InputStream {
        val classLoader: ClassLoader = javaClass.classLoader
        val inputStream = classLoader.getResourceAsStream(fileName)

        if(inputStream == null) {
            throw IllegalArgumentException("file not found! $fileName")
        } else {
            return inputStream
        }
    }

    fun getFileFromResource(fileName: String): File {
        val classLoader: ClassLoader = javaClass.classLoader
        val resource: URL? = classLoader.getResource(fileName)
        if(resource == null) {
            throw IllegalArgumentException("file not found! $fileName")
        } else {
            return File(resource.toURI())
        }
    }
}