package ch.asyncsystems.util

class Time(var hours: Int, var minutes: Int, var seconds: Int = 0)

fun difference(start: Time, end: Time): Time {
    val diff = Time(0,0,0)

    if(end.seconds > start.seconds) {
        --start.minutes
        start.seconds += 60
    }

    diff.seconds = start.seconds - end.seconds
    if(end.minutes > start.minutes) {
        --start.hours
        start.minutes += 60
    }

    diff.minutes = start.minutes - end.minutes
    diff.hours = start.hours - end.hours

    return diff
}