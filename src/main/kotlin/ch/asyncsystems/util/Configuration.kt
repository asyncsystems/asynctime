package ch.asyncsystems.util

import org.eclipse.microprofile.config.ConfigProvider

class Configuration {

    companion object {
        // MONGODB Credentials
        private var username: String = ConfigProvider.getConfig().getValue("mongodb.username", String::class.java)
        private var password: String = ConfigProvider.getConfig().getValue("mongodb.password", String::class.java)
        private var host: String = ConfigProvider.getConfig().getValue("mongodb.host", String::class.java)
        private var port: Int = ConfigProvider.getConfig().getValue("mongodb.port", Int::class.java)

        // Creditor Information
        private var account: String = ConfigProvider.getConfig().getValue("billInfo.account", String::class.java)
        private var currency: String = ConfigProvider.getConfig().getValue("billInfo.currency", String::class.java)

        private var creditorName: String = ConfigProvider.getConfig().getValue("billInfo.creditorName", String::class.java)
        private var creditorStreet: String = ConfigProvider.getConfig().getValue("billInfo.creditorStreet", String::class.java)
        private var creditorHouseNo: String = ConfigProvider.getConfig().getValue("billInfo.creditorHouseNo", String::class.java)
        private var creditorPostalCode: String = ConfigProvider.getConfig().getValue("billInfo.creditorPostalCode", String::class.java)
        private var creditorTown: String = ConfigProvider.getConfig().getValue("billInfo.creditorTown", String::class.java)
        private var creditorCountryCode: String = ConfigProvider.getConfig().getValue("billInfo.creditorCountryCode", String::class.java)

        fun getUsername(): String {
            return username
        }
        fun getPassword(): String {
            return password
        }
        fun getHost(): String {
            return host
        }
        fun getPort(): Int {
            return port
        }

        fun getAccount(): String {
            return account
        }
        fun getCurrency(): String {
            return currency
        }
        fun getCreditorName(): String {
            return creditorName
        }
        fun getCreditorStreet(): String {
            return creditorStreet
        }
        fun getCreditorHouseNo(): String {
            return creditorHouseNo
        }
        fun getCreditorPostalCode(): String {
            return creditorPostalCode
        }
        fun getCreditorTown(): String {
            return creditorTown
        }
        fun getCreditorCountryCode(): String {
            return creditorCountryCode
        }
    }

}