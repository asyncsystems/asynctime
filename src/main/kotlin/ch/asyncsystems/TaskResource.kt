package ch.asyncsystems

import ch.asyncsystems.repo.Task
import ch.asyncsystems.repo.TaskRepository
import ch.asyncsystems.repo.TaskType
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import kotlin.math.ceil
import kotlin.math.roundToInt

@Path("/tasks")
class TaskResource {

    /**
     * @return Returns all the tasks.
     * Returns a complete list of all the tasks from all the customers.
     */
    @GET
    fun getTasks(): List<Task> {
        return TaskRepository.list()
    }

    /**
     * @param id: Returns the task, with the specified id.
     * Returns the task, with the specified id.
     */
    @GET
    @Path("/{id}")
    fun getTaskByID(@PathParam("id") id: String): Task {
        return TaskRepository.listFilter("{_id: '${id}'}").first()
    }

    /**
     * @param jsonString: The jsonString, with the needed data of the new task.
     * Generates a new task, with the data, passed by the jsonString parameter.
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    fun addTask(jsonString: String) {

        val jsonObject = JSONObject(jsonString)

        val task: Task = when(jsonObject["type"].toString().lowercase()) {
            TaskType.SINGLE.type.lowercase() -> {
                Task(taskType = TaskType.SINGLE, customerID = jsonObject["customerID"].toString(), executionDate = jsonObject["date"].toString(), description = jsonObject["description"].toString(), singlePrice = jsonObject["singlePrice"].toString().toDouble(), quantity = jsonObject["quantity"].toString().toDouble(), vat = jsonObject["vat"].toString().toDouble(), total = roundOff((jsonObject["singlePrice"].toString().toDouble()*jsonObject["quantity"].toString().toDouble())+((jsonObject["singlePrice"].toString().toDouble()/100)*jsonObject["vat"].toString().toDouble())))
            }
            TaskType.TIMED.type.lowercase() -> {
                Task(taskType = TaskType.TIMED, customerID = jsonObject["customerID"].toString(), executionDate = jsonObject["date"].toString(), description = jsonObject["description"].toString(), singlePrice = jsonObject["singlePrice"].toString().toDouble(), timeStart = jsonObject["timeStart"].toString(), timeEnd = jsonObject["timeEnd"].toString(), vat = jsonObject["vat"].toString().toDouble(), total = roundOff((jsonObject["singlePrice"].toString().toDouble()*timeDiff(jsonObject["timeStart"].toString(), jsonObject["timeEnd"].toString()))+((jsonObject["singlePrice"].toString().toDouble()/100)*jsonObject["vat"].toString().toDouble())))
            }
            TaskType.CONTRACT.type.lowercase() -> {
                Task(taskType = TaskType.CONTRACT, customerID = jsonObject["customerID"].toString(), description = jsonObject["description"].toString(), singlePrice = jsonObject["singlePrice"].toString().toDouble(), contractStart = jsonObject["contractStart"].toString(), contractEnd = jsonObject["contractEnd"].toString(), vat = jsonObject["vat"].toString().toDouble(), total = roundOff((jsonObject["singlePrice"].toString().toDouble())+((jsonObject["singlePrice"].toString().toDouble()/100)*jsonObject["vat"].toString().toDouble())))
            }
            else -> {
                Task(taskType = TaskType.SINGLE, customerID = jsonObject["customerID"].toString(), description = jsonObject["description"].toString(), singlePrice = jsonObject["singlePrice"].toString().toDouble(), quantity = jsonObject["quantity"].toString().toDouble(), timeStart = jsonObject["timeStart"].toString(), timeEnd = jsonObject["timeEnd"].toString(), contractStart = jsonObject["contractStart"].toString(), contractEnd = jsonObject["contractEnd"].toString(), vat = jsonObject["vat"].toString().toDouble())
            }
        }
        TaskRepository.add(task)
    }

    /**
     * @param jsonString: The jsonString, with the needed data to update the task.
     * Updates the specified task, with the data of the passed jsonString parameter.
     */
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    fun updateTask(jsonString: String) {

        val jsonObject = JSONObject(jsonString)

        val type = jsonObject["type"].toString()

        val id = jsonObject["_id"].toString()
        val customerID = jsonObject["customerID"].toString()
        val executionDate = jsonObject["executionDate"].toString()
        val description = jsonObject["description"].toString()
        val singlePrice = jsonObject["singlePrice"].toString().toDouble()
        val quantity = jsonObject["quantity"].toString().toDouble()
        val timeStart = jsonObject["timeStart"].toString()
        val timeEnd = jsonObject["timeEnd"].toString()
        val contractStart = jsonObject["contractStart"].toString()
        val contractEnd = jsonObject["contractEnd"].toString()
        val vat = jsonObject["vat"].toString().toDouble()

        val task: Task = when(type.lowercase()) {
            TaskType.SINGLE.type.lowercase() -> {
                Task(_id = id, taskType = TaskType.SINGLE, customerID = customerID, executionDate = executionDate, description = description, singlePrice = singlePrice, quantity = quantity, vat = vat, total = roundOff((singlePrice*quantity!!)+((singlePrice/100)*vat)))
            }
            TaskType.TIMED.type.lowercase() -> {
                Task(_id = id, taskType = TaskType.TIMED, customerID = customerID, executionDate = executionDate, description = description, singlePrice = singlePrice, timeStart = timeStart, timeEnd = timeEnd, vat = vat, total = roundOff((singlePrice*timeDiff(timeStart!!, timeEnd!!))+((singlePrice/100)*vat)))
            }
            TaskType.CONTRACT.type.lowercase() -> {
                Task(_id = id, taskType = TaskType.CONTRACT, customerID = customerID, description = description, singlePrice = singlePrice, contractStart = contractStart, contractEnd = contractEnd, vat = vat, total = roundOff((singlePrice)+((singlePrice/100)*vat)))
            }
            else -> {
                Task(_id = id, taskType = TaskType.SINGLE, customerID = customerID, description = description, singlePrice = singlePrice, quantity = quantity, timeStart = timeStart, timeEnd = timeEnd, contractStart = contractStart, contractEnd = contractEnd, vat = vat)
            }
        }
        TaskRepository.update(task)
    }

    /**
     * @param _id: Form-Paramter, passed by a HTMLForm
     * Deletes the paramter-specified task from the database.
     */
    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    fun deleteTask(@FormParam("_id") _id: String) {
        val taskToDelete = TaskRepository.getById(_id)
        if(taskToDelete != null) {
            TaskRepository.delete(taskToDelete)
        }
    }

    private fun round(num: Double): Double {
        return ceil((num / 20) * 20)
    }

    private fun timeDiff(timeStart: String, timeEnd: String): Double {
        val timeFormat = SimpleDateFormat("hh:mm")

        val start = timeFormat.parse(timeStart)
        val end = timeFormat.parse(timeEnd)

        val longDifference = end.time - start.time
        val hoursDifference = (longDifference / (1000*60*60)) % 24
        val minutesDifference = (longDifference / (1000*60)) % 60

        return DecimalFormat("#,##0.00").format(hoursDifference.toDouble()+(minutesDifference*60 + 24)/(60*60).toDouble()-0.01).toDouble()
    }

    private fun roundOff(value: Double): Double {
        return BigDecimal((value / 0.05).roundToInt() * 0.05).setScale(2, RoundingMode.HALF_EVEN).toDouble()
    }

}