import React from 'react';
import ReactDOM from 'react-dom';

import '@patternfly/patternfly/patternfly.css';
import App from './App';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
