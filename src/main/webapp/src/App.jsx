import React from 'react';
import { Router } from "@reach/router"

import Layout from './components/Layout';
import Particles from './components/particles';
import Tasks from './components/tasks';
import Customers from './components/customers';
import Bills from './components/bills'

const App = () => {

  return (
    <Layout>
      <Router>
        <Particles path="/" />
        <Tasks path="/task" />
        <Customers path="/customer" />
        <Bills path="/bill" />
      </Router>
    </Layout>
  );
}

export default App;
