import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "@popperjs/core/dist/umd/popper.min.js";
import "./scss/header.scss";

const Header = ({children,uri}) => {
    return (
        <>
        <div className="top-header"></div>
        <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="nav hideme">
                    {React.Children.map(children, (child) => (
                    <div className="nav-link">{child}</div>
                    ))}
                </div>

                <ul className="navbar-nav ml-5 mr-auto">
                    <input type="checkbox" className="openSidebarMenu" id="openSidebarMenu" />
                    <label htmlFor="openSidebarMenu" className="sidebarIconToggle">
                    <div className="spinner diagonal part-1"></div>
                    <div className="spinner horizontal"></div>
                    <div className="spinner diagonal part-2"></div>
                    </label>
                    <div id="sidebarMenu">
                    <ul className="sidebarMenuInner">
                        {React.Children.map(children, (child) => (
                        <li>{child}</li>
                        ))}
                    </ul>
                    </div>
                </ul>
            </nav>
      </header>
    </>
    )
}

export default Header;