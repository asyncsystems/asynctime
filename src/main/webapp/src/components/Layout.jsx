import React from "react";
import { Link } from "@reach/router";

import Header from "./Header";

const Layout = ({ children }) => {
    return (
        <>
            <Header>
                <Link to="/">Home</Link>
                <Link to="/task">Zeiterfassung</Link>
                <Link to="/customer">Kundenregister</Link>
                <Link to="/bill">Rechnungen</Link>
            </Header>
            <main>
                {children}
            </main>
        </>
    )
}

export default Layout;