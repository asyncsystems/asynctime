import React, { useState } from 'react';
import axios from 'axios';

const Customers = () => {

    const [customers, setCustomers] = useState([])
    const [toDeleteCustomer, setToDeleteCustomer] = useState()

    React.useEffect(() => {
        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/customers",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setCustomers(data.data)
        }).catch((err) => console.error(err))

        /*fetch('/customers')
        .then(res => res.json())
        .then((data) => {
            setCustomers(data)
        })
        .catch(console.log)*/
    }, [])

    function hidePrivateCustomer() {
        let privateCustomer = document.getElementById("privateCustomer")
        let privateCustomerBtn = document.getElementById("privateCustomerBtn")
        privateCustomer.classList = "d-none"
        privateCustomerBtn.classList = "nav-link"

        let companyCustomer = document.getElementById("companyCustomer")
        let companyCustomerBtn = document.getElementById("companyCustomerBtn")
        companyCustomer.classList = ""
        companyCustomerBtn.classList = "nav-link active"
    }

    function hideCompanyCustomer() {
        let privateCustomer = document.getElementById("privateCustomer")
        let privateCustomerBtn = document.getElementById("privateCustomerBtn")
        privateCustomer.classList = ""
        privateCustomerBtn.classList = "nav-link active"

        let companyCustomer = document.getElementById("companyCustomer")
        let companyCustomerBtn = document.getElementById("companyCustomerBtn")
        companyCustomer.classList = "d-none"
        companyCustomerBtn.classList = "nav-link"
    }

    function hidePrivateCustomerList() {
        let privateCustomer = document.getElementById("privateCustomerList")
        let privateCustomerBtn = document.getElementById("privateCustomerListBtn")
        privateCustomer.classList = "d-none"
        privateCustomerBtn.classList = "nav-link"

        let companyCustomer = document.getElementById("companyCustomerList")
        let companyCustomerBtn = document.getElementById("companyCustomerListBtn")
        companyCustomer.classList = ""
        companyCustomerBtn.classList = "nav-link active"
    }

    function hideCompanyCustomerList() {
        let privateCustomer = document.getElementById("privateCustomerList")
        let privateCustomerBtn = document.getElementById("privateCustomerListBtn")
        privateCustomer.classList = ""
        privateCustomerBtn.classList = "nav-link active"

        let companyCustomer = document.getElementById("companyCustomerList")
        let companyCustomerBtn = document.getElementById("companyCustomerListBtn")
        companyCustomer.classList = "d-none"
        companyCustomerBtn.classList = "nav-link"
    }
    
    return (
        <div className="container">
            <center><h1>Kunden</h1></center>

            <button type="button" className="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#addCustomerModal">
                Neuer Kunde
            </button>

            <div className="modal fade" id="addCustomerModal" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="addCustomerModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                        <div className="modal-content">
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <button type="button" className="nav-link active" aria-current="page" onClick={() => hideCompanyCustomer()} id="privateCustomerBtn">Privatkunde</button>
                                </li>
                                <li className="nav-item">
                                    <button type="button" className="nav-link" onClick={() => hidePrivateCustomer()} id="companyCustomerBtn">Firmenkunde</button>
                                </li>
                            </ul>
                            <div id="privateCustomer">
                                <form id="faddPrivateCustomer" onSubmit={(event) => {
                                    event.preventDefault();

                                    const data = new FormData(event.target);
                                    const value = Object.fromEntries(data.entries());

                                    var config = {
                                        headers: {
                                            'Content-Type': 'text/plain'
                                        }
                                    };
                                    axios.post("http://127.0.0.1:52445/customers", JSON.stringify(value), config)

                                    document.getElementById("faddPrivateCustomer").reset();
                                }}>
                                    <input type="text" defaultValue="private" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="addCustomerModalLabel">Privatkunde</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="salutSelect" name="salut">
                                                        <option value="">Wähle</option>
                                                        <option value="Herr">Herr</option>
                                                        <option value="Frau">Frau</option>
                                                        <option value="Divers">Divers</option>
                                                    </select>
                                                    <label htmlFor="salutSelect">Anrede</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="name" id="Name" placeholder="Name" />
                                                    <label htmlFor="Name">Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    <input type="phone" className="form-control" name="handy" id="Handy" placeholder="Handy" />
                                                    <label htmlFor="Handy">Handy</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="email" className="form-control" name="email" id="E-Mail" placeholder="E-Mail" />
                                                    <label htmlFor="E-Mail">E-Mail</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="street" id="Strasse" placeholder="Strasse" />
                                                    <label htmlFor="Strasse">Strasse</label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="houseNo" id="Hausnr" placeholder="Hausnr" />
                                                    <label htmlFor="Hausnr">Hausnr</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="postalCode" id="PLZ" placeholder="PLZ" />
                                                    <label htmlFor="PLZ">PLZ</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="town" id="Stadt" placeholder="Stadt" />
                                                    <label htmlFor="Stadt">Stadt</label>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="countryCode" id="Landeskürzel" placeholder="Landeskürzel" />
                                                    <label htmlFor="Landeskürzel">Landeskürzel</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Kunde hinzufügen</button>
                                    </div>
                                </form>
                            </div>
                            <div id="companyCustomer" className="d-none">
                                <form id="faddCompanyCustomer" onSubmit={(event) => {
                                    event.preventDefault();

                                    const data = new FormData(event.target);
                                    const value = Object.fromEntries(data.entries());

                                    var config = {
                                        headers: {
                                            'Content-Type': 'text/plain'
                                        }
                                    };
                                    axios.post("http://127.0.0.1:52445/customers", JSON.stringify(value), config)

                                    document.getElementById("faddCompanyCustomer").reset();
                                }}>
                                    <input type="text" defaultValue="company" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="addCustomerModalLabel">Firmenkunde</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="salutSelect" name="salut">
                                                        <option value="">Wähle</option>
                                                        <option value="Herr">Herr</option>
                                                        <option value="Frau">Frau</option>
                                                        <option value="Divers">Divers</option>
                                                    </select>
                                                    <label htmlFor="salutSelect">Anrede</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="name" id="Name" placeholder="Name" />
                                                    <label htmlFor="Name">Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    <input type="phone" className="form-control" name="handy" id="Handy" placeholder="Handy" />
                                                    <label htmlFor="Handy">Handy</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="email" className="form-control" name="email" id="E-Mail" placeholder="E-Mail" />
                                                    <label htmlFor="E-Mail">E-Mail</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="companyName" id="companyName" placeholder="Firmenname" />
                                                    <label htmlFor="companyName">Firmenname</label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="legalFormSelect" name="legalForm">
                                                        <option value="Einzelfirma">Einzelfirma</option>
                                                        <option value="GmbH">GmbH</option>
                                                        <option value="AG">AG</option>
                                                        <option value="Co">Kollektivgesellschaft</option>
                                                        <option value="Verein">Verein</option>
                                                        <option value="Zweigniederlassung">Zweigniederlassung</option>
                                                        <option value="Stiftung">Stiftung</option>
                                                        <option value="Genossenschaft">Genossenschaft</option>
                                                        <option value="Sonstige">Sonstige</option>
                                                    </select>
                                                    <label htmlFor="salutSelect">Anrede</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="street" id="Strasse" placeholder="Strasse" />
                                                    <label htmlFor="Strasse">Strasse</label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="houseNo" id="Hausnr" placeholder="Hausnr" />
                                                    <label htmlFor="Hausnr">Hausnr</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="postalCode" id="PLZ" placeholder="PLZ" />
                                                    <label htmlFor="PLZ">PLZ</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="town" id="Stadt" placeholder="Stadt" />
                                                    <label htmlFor="Stadt">Stadt</label>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="countryCode" id="Landeskürzel" placeholder="Landeskürzel" />
                                                    <label htmlFor="Landeskürzel">Landeskürzel</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Kunde hinzufügen</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>

            <div className="modal fade" id="deleteTaskModal" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="deleteTaskModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form action="/customers/delete" method="POST">
                            <div className="row">
                                {toDeleteCustomer === undefined ? (
                                    <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" />
                                ) : (
                                    <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" defaultValue={toDeleteCustomer} />
                                )}
                                <div className="col">
                                    <p className="text-center">Möchten Sie den Kunden wirklich löschen?</p>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-danger" data-bs-dismiss="modal">Löschen</button>
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <button type="button" className="nav-link active" aria-current="page" onClick={() => hideCompanyCustomerList()} id="privateCustomerListBtn">Privatkunden</button>
                </li>
                <li className="nav-item">
                    <button type="button" className="nav-link" onClick={() => hidePrivateCustomerList()} id="companyCustomerListBtn">Firmenkunden</button>
                </li>
            </ul>
            <div id="privateCustomerList">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Anrede</th>
                            <th scope="col">Name</th>
                            <th scope="col">Handy-Nummer</th>
                            <th scope="col">E-Mail Adresse</th>
                            <th scope="col">Strasse</th>
                            <th scope="col">Hausnr.</th>
                            <th scope="col">PLZ</th>
                            <th scope="col">Stadt</th>
                            <th scope="col">Landeskürzel</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer) => (
                            <>
                                {customer.customerType === "PRIVATE" ? (
                                    <tr key={customer._id}>
                                        <td>{customer.salut}</td>
                                        <td>{customer.name}</td>
                                        <td>{customer.handy}</td>
                                        <td>{customer.email}</td>
                                        <td>{customer.street}</td>
                                        <td>{customer.houseNo}</td>
                                        <td>{customer.postalCode}</td>
                                        <td>{customer.town}</td>
                                        <td>{customer.countryCode}</td>
                                        <td>
                                            <button type="button" className="btn btn-danger mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#deleteTaskModal" onClick={() => setToDeleteCustomer(customer._id)}>Löschen</button>   
                                        </td>
                                    </tr>
                                ) : (
                                    <tr></tr>
                                )}
                            </>
                        ))}
                    </tbody>
                </table>
            </div>
            <div id="companyCustomerList" className="d-none">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Anrede</th>
                            <th scope="col">Kontaktperson</th>
                            <th scope="col">Firma</th>
                            <th scope="col">Rechtsform</th>
                            <th scope="col">Handy-Nummer</th>
                            <th scope="col">E-Mail Adresse</th>
                            <th scope="col">Strasse</th>
                            <th scope="col">Hausnr.</th>
                            <th scope="col">PLZ</th>
                            <th scope="col">Stadt</th>
                            <th scope="col">Landeskürzel</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer) => (
                            <>
                            {customer.customerType === "COMPANY" ? (
                                <tr key={customer._id}>
                                    <td>{customer.salut}</td>
                                    <td>{customer.name}</td>
                                    <td>{customer.companyName}</td>
                                    <td>{customer.legalForm}</td>
                                    <td>{customer.handy}</td>
                                    <td>{customer.email}</td>
                                    <td>{customer.street}</td>
                                    <td>{customer.houseNo}</td>
                                    <td>{customer.postalCode}</td>
                                    <td>{customer.town}</td>
                                    <td>{customer.countryCode}</td>
                                    <td>
                                        <button type="button" className="btn btn-danger mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#deleteTaskModal" onClick={() => setToDeleteCustomer(customer._id)}>Löschen</button>   
                                    </td>
                                </tr>
                            ) : (
                                <tr></tr>
                            )}
                            </>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Customers