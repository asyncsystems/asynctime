import React, { useState } from 'react';
import moment from 'moment';
import axios from 'axios';

const Tasks = () => {

    const [tasks, setTasks] = useState([])
    const [customers, setCustomers] = useState([])
    const [toUpdateTask, setToUpdateTask] = useState()
    const [toDeleteTask, setToDeleteTask] = useState()

    React.useEffect(() => {
        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/tasks",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setTasks(data.data)
        }).catch((err) => console.error(err))

        /*fetch('/tasks')
        .then(res => res.json())
        .then((data) => {
            setTasks(data)
        })
        .catch(console.log)*/

        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/customers",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setCustomers(data.data)
        }).catch((err) => console.error(err))

        /*fetch('/customers')
        .then(res => res.json())
        .then((data) => {
            setCustomers(data)
        })
        .catch(console.log)*/
    }, [])

    function updateTask(id) {

        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/tasks/"+id,
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setToUpdateTask(data.data)
        }).catch((err) => console.error(err))

        /*fetch('/tasks/'+id)
        .then(res => res.json())
        .then((data) => {
            console.log(data)
            setToUpdateTask(data)
        })
        .catch(console.log)*/
    }

    function timeDiff(starttime, endtime) {
        let start = moment(starttime, 'HH:mm')
        let end = moment(endtime, 'HH:mm')

        let d = moment.duration(moment(endtime, 'HH:mm:ss a').diff(moment(starttime, 'HH:mm:ss a')))
        let h = end.diff(start, 'hours')
        
        let min = parseInt(d.asMinutes()) % 60
        if(min.toString().length == 1) {
            return h+":"+min+"0"
        }
        return h+":"+min
    }

    function showSingleTask() {
        let singleTask = document.getElementById("singleTask")
        let singleTaskBtn = document.getElementById("singleTaskBtn")
        singleTask.classList = ""
        singleTaskBtn.classList = "nav-link active"

        let timedTask = document.getElementById("timedTask")
        let timedTaskBtn = document.getElementById("timedTaskBtn")
        timedTask.classList = "d-none"
        timedTaskBtn.classList = "nav-link"

        let contractTask = document.getElementById("contractTask")
        let contractTaskBtn = document.getElementById("contractTaskBtn")
        contractTask.classList = "d-none"
        contractTaskBtn.classList = "nav-link"
    }

    function showTimedTask() {
        let singleTask = document.getElementById("singleTask")
        let singleTaskBtn = document.getElementById("singleTaskBtn")
        singleTask.classList = "d-none"
        singleTaskBtn.classList = "nav-link"

        let timedTask = document.getElementById("timedTask")
        let timedTaskBtn = document.getElementById("timedTaskBtn")
        timedTask.classList = ""
        timedTaskBtn.classList = "nav-link active"

        let contractTask = document.getElementById("contractTask")
        let contractTaskBtn = document.getElementById("contractTaskBtn")
        contractTask.classList = "d-none"
        contractTaskBtn.classList = "nav-link"
    }

    function showServiceTask() {
        let singleTask = document.getElementById("singleTask")
        let singleTaskBtn = document.getElementById("singleTaskBtn")
        singleTask.classList = "d-none"
        singleTaskBtn.classList = "nav-link"

        let timedTask = document.getElementById("timedTask")
        let timedTaskBtn = document.getElementById("timedTaskBtn")
        timedTask.classList = "d-none"
        timedTaskBtn.classList = "nav-link"

        let contractTask = document.getElementById("contractTask")
        let contractTaskBtn = document.getElementById("contractTaskBtn")
        contractTask.classList = ""
        contractTaskBtn.classList = "nav-link active"
    }

    function showsingleTaskList() {
        let singleTaskList = document.getElementById("singleTaskList")
        let singleTaskListBtn = document.getElementById("singleTaskListBtn")
        singleTaskList.classList = ""
        singleTaskListBtn.classList = "nav-link active"

        let timedTaskList = document.getElementById("timedTaskList")
        let timedTaskListBtn = document.getElementById("timedTaskListBtn")
        timedTaskList.classList = "d-none"
        timedTaskListBtn.classList = "nav-link"

        let serviceTaskList = document.getElementById("serviceTaskList")
        let serviceTaskListBtn = document.getElementById("serviceTaskListBtn")
        serviceTaskList.classList = "d-none"
        serviceTaskListBtn.classList = "nav-link"
    }

    function showtimedTaskList() {
        let singleTaskList = document.getElementById("singleTaskList")
        let singleTaskListBtn = document.getElementById("singleTaskListBtn")
        singleTaskList.classList = "d-none"
        singleTaskListBtn.classList = "nav-link"

        let timedTaskList = document.getElementById("timedTaskList")
        let timedTaskListBtn = document.getElementById("timedTaskListBtn")
        timedTaskList.classList = ""
        timedTaskListBtn.classList = "nav-link active"

        let serviceTaskList = document.getElementById("serviceTaskList")
        let serviceTaskListBtn = document.getElementById("serviceTaskListBtn")
        serviceTaskList.classList = "d-none"
        serviceTaskListBtn.classList = "nav-link"
    }

    function showserviceTaskList() {
        let singleTaskList = document.getElementById("singleTaskList")
        let singleTaskListBtn = document.getElementById("singleTaskListBtn")
        singleTaskList.classList = "d-none"
        singleTaskListBtn.classList = "nav-link"

        let timedTaskList = document.getElementById("timedTaskList")
        let timedTaskListBtn = document.getElementById("timedTaskListBtn")
        timedTaskList.classList = "d-none"
        timedTaskListBtn.classList = "nav-link"

        let serviceTaskList = document.getElementById("serviceTaskList")
        let serviceTaskListBtn = document.getElementById("serviceTaskListBtn")
        serviceTaskList.classList = ""
        serviceTaskListBtn.classList = "nav-link active"
    }

    function showEditSingleTask() {
        let singleTask = document.getElementById("editSingleTask")
        let singleTaskBtn = document.getElementById("editSingleTaskBtn")
        singleTask.classList = ""
        singleTaskBtn.classList = "nav-link active"

        let timedTask = document.getElementById("editTimedTask")
        let timedTaskBtn = document.getElementById("editTimedTaskBtn")
        timedTask.classList = "d-none"
        timedTaskBtn.classList = "nav-link"

        let contractTask = document.getElementById("editContractTask")
        let contractTaskBtn = document.getElementById("editContractTaskBtn")
        contractTask.classList = "d-none"
        contractTaskBtn.classList = "nav-link"
    }

    function showEditTimedTask() {
        let singleTask = document.getElementById("editSingleTask")
        let singleTaskBtn = document.getElementById("editSingleTaskBtn")
        singleTask.classList = "d-none"
        singleTaskBtn.classList = "nav-link"

        let timedTask = document.getElementById("editTimedTask")
        let timedTaskBtn = document.getElementById("editTimedTaskBtn")
        timedTask.classList = ""
        timedTaskBtn.classList = "nav-link active"

        let contractTask = document.getElementById("editContractTask")
        let contractTaskBtn = document.getElementById("editContractTaskBtn")
        contractTask.classList = "d-none"
        contractTaskBtn.classList = "nav-link"
    }

    function showEditServiceTask() {
        let singleTask = document.getElementById("editSingleTask")
        let singleTaskBtn = document.getElementById("editSingleTaskBtn")
        singleTask.classList = "d-none"
        singleTaskBtn.classList = "nav-link"

        let timedTask = document.getElementById("editTimedTask")
        let timedTaskBtn = document.getElementById("editTimedTaskBtn")
        timedTask.classList = "d-none"
        timedTaskBtn.classList = "nav-link"

        let contractTask = document.getElementById("editContractTask")
        let contractTaskBtn = document.getElementById("editContractTaskBtn")
        contractTask.classList = ""
        contractTaskBtn.classList = "nav-link active"
    }
    
    return (
        <div className="container">
            <center><h1>Aufträge</h1></center>

            <button type="button" className="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#addTaskModal">
                Neue Zeiterfassung
            </button>

            <div className="modal fade" id="addTaskModal" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="addTaskModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                        <div className="modal-content">
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <button type="button" className="nav-link active" aria-current="page" onClick={() => showSingleTask()} id="singleTaskBtn">Einzelauftrag</button>
                                </li>
                                <li className="nav-item">
                                    <button type="button" className="nav-link" onClick={() => showTimedTask()} id="timedTaskBtn">Zeitauftrag</button>
                                </li>
                                <li className="nav-item">
                                    <button type="button" className="nav-link" onClick={() => showServiceTask()} id="contractTaskBtn">Servicevertrag</button>
                                </li>
                            </ul>
                            <div id="singleTask">
                                <form id="faddSingleTask" onSubmit={(event) => {
                                    event.preventDefault();

                                    const data = new FormData(event.target);
                                    const value = Object.fromEntries(data.entries());

                                    var config = {
                                        headers: {
                                            'Content-Type': 'text/plain'
                                        }
                                    };
                                    axios.post("http://127.0.0.1:52445/tasks", JSON.stringify(value), config)
                                    document.getElementById("faddSingleTask").reset();
                                }}>
                                    <input type="text" defaultValue="single" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="addTaskModalLabel">Einzelauftrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                            <>
                                                                {customer.customerType === "PRIVATE" ? (
                                                                    <option value={customer._id}>{customer.name}</option>
                                                                ) : (
                                                                    <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                )}
                                                            </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    <input type="date" className="form-control" name="date" id="Datum" placeholder="Beschreibung" />
                                                    <label htmlFor="Datum">Datum</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="number" className="form-control" name="singlePrice" id="Einzelpreis" step="0.01" placeholder="Einzelpreis" />
                                                    <label htmlFor="Einzelpreis">Einzelpreis</label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    <input type="number" className="form-control" defaultValue="1" name="quantity" id="Anzahl" placeholder="Anzahl" />
                                                    <label htmlFor="Anzahl">Anzahl</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Auftrag hinzufügen</button>
                                    </div>
                                </form>
                            </div>
                            <div id="timedTask" className="d-none">
                                <form id="faddTimedTask" onSubmit={(event) => {
                                    event.preventDefault();

                                    const data = new FormData(event.target);
                                    const value = Object.fromEntries(data.entries());

                                    var config = {
                                        headers: {
                                            'Content-Type': 'text/plain'
                                        }
                                    };
                                    axios.post("http://127.0.0.1:52445/tasks", JSON.stringify(value), config)
                                    document.getElementById("faddTimedTask").reset();
                                }}>
                                    <input type="text" defaultValue="timed" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="addTaskModalLabel">Zeitauftrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                            <>
                                                                {customer.customerType === "PRIVATE" ? (
                                                                    <option value={customer._id}>{customer.name}</option>
                                                                ) : (
                                                                    <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                )}
                                                            </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    <input type="date" className="form-control" name="date" id="Datum" placeholder="Beschreibung" />
                                                    <label htmlFor="Datum">Datum</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="number" className="form-control" name="singlePrice" id="Stundenpreis" step="0.01" placeholder="Stundenpreis" />
                                                    <label htmlFor="Stundenpreis">Stundenpreis</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="time" className="form-control" name="timeStart" id="Startzeit" placeholder="Startzeit" />
                                                    <label htmlFor="Startzeit">Startzeit</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="time" className="form-control" name="timeEnd" id="Endzeit" placeholder="Endzeit" />
                                                    <label htmlFor="Endzeit">Endzeit</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Auftrag hinzufügen</button>
                                    </div>
                                </form>
                            </div>
                            <div id="contractTask" className="d-none">
                                <form id="faddContractTask" onSubmit={(event) => {
                                    event.preventDefault();

                                    const data = new FormData(event.target);
                                    const value = Object.fromEntries(data.entries());

                                    var config = {
                                        headers: {
                                            'Content-Type': 'text/plain'
                                        }
                                    };
                                    axios.post("http://127.0.0.1:52445/tasks", JSON.stringify(value), config)
                                    document.getElementById("faddContractTask").reset();
                                }}>
                                    <input type="text" defaultValue="contract" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="addTaskModalLabel">Servicevertrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                            <>
                                                                {customer.customerType === "PRIVATE" ? (
                                                                    <option value={customer._id}>{customer.name}</option>
                                                                ) : (
                                                                    <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                )}
                                                            </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-floating mb-3">
                                                    <input type="number" className="form-control" name="singlePrice" id="Preis" step="0.01" placeholder="Preis" />
                                                    <label htmlFor="Preis">Preis</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="date" className="form-control" name="contractStart" id="Start" placeholder="Start" />
                                                    <label htmlFor="Start">Start</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="date" className="form-control" name="contractEnd" id="Ende" placeholder="Ende" />
                                                    <label htmlFor="Ende">Ende</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Vertrag hinzufügen</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>

            <div className="modal fade" id="editTaskModal" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="editTaskModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                        <div className="modal-content">
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <button type="button" className="nav-link active" aria-current="page" onClick={() => showEditSingleTask()} id="editSingleTaskBtn">Einzelauftrag</button>
                                </li>
                                <li className="nav-item">
                                    <button type="button" className="nav-link" onClick={() => showEditTimedTask()} id="editTimedTaskBtn">Zeitauftrag</button>
                                </li>
                                <li className="nav-item">
                                    <button type="button" className="nav-link" onClick={() => showEditServiceTask()} id="editContractTaskBtn">Servicevertrag</button>
                                </li>
                            </ul>
                            <div id="editSingleTask">
                                <form action="/tasks/update" method="POST">
                                    <input type="text" defaultValue="single" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="editTaskModalLabel">Einzelauftrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                            <>
                                                                {customer.customerType === "PRIVATE" ? (
                                                                    <>
                                                                    {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                        <option value={customer._id} selected>{customer.name}</option>
                                                                    ) : (
                                                                        <option value={customer._id}>{customer.name}</option>
                                                                    )}
                                                                    </>
                                                                ) : (
                                                                    <>
                                                                    {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                        <option value={customer._id} selected>{customer.companyName} {customer.legalForm}</option>
                                                                    ) : (
                                                                        <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                    )}
                                                                    </>
                                                                    
                                                                )}
                                                            </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        {toUpdateTask === undefined ? (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" />
                                        ) : (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" defaultValue={toUpdateTask._id} />
                                        )}
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="date" className="form-control" name="date" id="Datum" placeholder="Beschreibung" />    
                                                    ) : (
                                                        <input type="date" className="form-control" name="date" id="Datum" placeholder="Beschreibung" defaultValue={toUpdateTask.executionDate} />    
                                                    )}
                                                    <label htmlFor="Datum">Datum</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                        ) : (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" defaultValue={toUpdateTask.description} />
                                                    )}
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" className="form-control" name="singlePrice" id="Einzelpreis" step="0.01" placeholder="Einzelpreis" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="singlePrice" id="Einzelpreis" step="0.01" placeholder="Einzelpreis" defaultValue={toUpdateTask.singlePrice} />
                                                    )}
                                                    
                                                    <label htmlFor="Einzelpreis">Einzelpreis</label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" className="form-control" defaultValue="1" name="quantity" id="Anzahl" placeholder="Anzahl" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="quantity" id="Anzahl" placeholder="Anzahl" defaultValue={toUpdateTask.quantity} />
                                                    )}
                                                    <label htmlFor="Anzahl">Anzahl</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" defaultValue={toUpdateTask.vat} />
                                                    )}
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Auftrag bearbeiten</button>
                                    </div>
                                </form>
                            </div>
                            <div id="editTimedTask" className="d-none">
                                <form action="/tasks/update" method="POST">
                                    <input type="text" defaultValue="timed" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="editTaskModalLabel">Zeitauftrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                             <>
                                                             {customer.customerType === "PRIVATE" ? (
                                                                 <>
                                                                 {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                     <option value={customer._id} selected>{customer.name}</option>
                                                                 ) : (
                                                                     <option value={customer._id}>{customer.name}</option>
                                                                 )}
                                                                 </>
                                                             ) : (
                                                                 <>
                                                                 {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                     <option value={customer._id} selected>{customer.companyName} {customer.legalForm}</option>
                                                                 ) : (
                                                                     <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                 )}
                                                                 </>
                                                                 
                                                             )}
                                                         </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        {toUpdateTask === undefined ? (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" />
                                        ) : (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" defaultValue={toUpdateTask._id} />
                                        )}
                                        <div className="row">
                                            <div className="col-5">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="date" className="form-control" name="date" id="Datum" placeholder="Datum" />
                                                    ) : (
                                                        <input type="date" className="form-control" name="date" id="Datum" placeholder="Datum" defaultValue={toUpdateTask.executionDate} />
                                                    )}
                                                    <label htmlFor="Datum">Datum</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                    ) : (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" defaultValue={toUpdateTask.description} />
                                                    )}
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" className="form-control" name="singlePrice" id="Stundenpreis" step="0.01" placeholder="Stundenpreis" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="singlePrice" id="Stundenpreis" step="0.01" placeholder="Stundenpreis" defaultValue={toUpdateTask.singlePrice} />
                                                    )}
                                                    <label htmlFor="Stundenpreis">Stundenpreis</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="time" className="form-control" name="timeStart" id="Startzeit" placeholder="Startzeit" />
                                                    ) : (
                                                        <input type="time" className="form-control" name="timeStart" id="Startzeit" placeholder="Startzeit" defaultValue={toUpdateTask.timeStart} />
                                                    )}
                                                    <label htmlFor="Startzeit">Startzeit</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="time" className="form-control" name="timeEnd" id="Endzeit" placeholder="Endzeit" />
                                                    ) : (
                                                        <input type="time" className="form-control" name="timeEnd" id="Endzeit" placeholder="Endzeit" defaultValue={toUpdateTask.timeEnd} />
                                                    )}
                                                    <label htmlFor="Endzeit">Endzeit</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" defaultValue={toUpdateTask.vat} />
                                                    )}
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Auftrag bearbeiten</button>
                                    </div>
                                </form>
                            </div>
                            <div id="editContractTask" className="d-none">
                                <form action="/tasks/update" method="POST">
                                    <input type="text" defaultValue="contract" name="type" className="d-none" readOnly />
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="editTaskModalLabel">Servicevertrag</h5>
                                        <button type="reset" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <select className="form-select" id="Kunde" name="customerID">
                                                        {customers.map((customer) => (
                                                             <>
                                                             {customer.customerType === "PRIVATE" ? (
                                                                 <>
                                                                 {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                     <option value={customer._id} selected>{customer.name}</option>
                                                                 ) : (
                                                                     <option value={customer._id}>{customer.name}</option>
                                                                 )}
                                                                 </>
                                                             ) : (
                                                                 <>
                                                                 {toUpdateTask !== undefined && toUpdateTask._id === customer._id ? (
                                                                     <option value={customer._id} selected>{customer.companyName} {customer.legalForm}</option>
                                                                 ) : (
                                                                     <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                                 )}
                                                                 </>
                                                                 
                                                             )}
                                                         </>
                                                        ))}
                                                    </select>
                                                    <label htmlFor="Kunde">Kunde</label>
                                                </div>
                                            </div>
                                        </div>
                                        {toUpdateTask === undefined ? (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" />
                                        ) : (
                                            <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" defaultValue={toUpdateTask._id} />
                                        )}
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" />
                                                    ) : (
                                                        <input type="text" className="form-control" name="description" id="Beschreibung" placeholder="Beschreibung" defaultValue={toUpdateTask.description} />
                                                    )}
                                                    <label htmlFor="Beschreibung">Beschreibung</label>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" className="form-control" name="singlePrice" id="Preis" step="0.01" placeholder="Preis" />
                                                    ) : (
                                                        <input type="number" className="form-control" name="singlePrice" id="Preis" step="0.01" placeholder="Preis" defaultValue={toUpdateTask.singlePrice}/>
                                                    )}
                                                    <label htmlFor="Preis">Preis</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="date" className="form-control" name="contractStart" id="Start" placeholder="Start" />
                                                    ) : (
                                                        <input type="date" className="form-control" name="contractStart" id="Start" placeholder="Start" defaultValue={toUpdateTask.contractStart}/>
                                                    )}
                                                    <label htmlFor="Start">Start</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="date" className="form-control" name="contractEnd" id="Ende" placeholder="Ende" />
                                                    ) : (
                                                        <input type="date" className="form-control" name="contractEnd" id="Ende" placeholder="Ende" defaultValue={toUpdateTask.contractEnd}/>
                                                    )}
                                                    <label htmlFor="Ende">Ende</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    {toUpdateTask === undefined ? (
                                                        <input type="number" defaultValue="7.7" className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" />
                                                    ) : (
                                                        <input type="number"className="form-control" name="vat" id="MwSt" step="0.1" placeholder="MwSt" defaultValue={toUpdateTask.vat} />
                                                    )}
                                                    <label htmlFor="MwSt">MwSt.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="reset" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                                        <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Vertrag bearbeiten</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>

            <div className="modal fade" id="deleteTaskModal" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="deleteTaskModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form action="/tasks/delete" method="POST">
                            <div className="row">
                                {toDeleteTask === undefined ? (
                                    <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" />
                                ) : (
                                    <input type="text" className="invisible" name="_id" id="ID" placeholder="ID" defaultValue={toDeleteTask} />
                                )}
                                <div className="col">
                                    <p className="text-center">Möchten Sie den Auftrag wirklich löschen?</p>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-danger" data-bs-dismiss="modal">Löschen</button>
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Schliessen</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <button type="button" className="nav-link active" aria-current="page" onClick={() => showsingleTaskList()} id="singleTaskListBtn">Einzelaufträge</button>
                </li>
                <li className="nav-item">
                    <button type="button" className="nav-link" onClick={() => showtimedTaskList()} id="timedTaskListBtn">Zeitaufträge</button>
                </li>
                <li className="nav-item">
                    <button type="button" className="nav-link" onClick={() => showserviceTaskList()} id="serviceTaskListBtn">Servicevertäge</button>
                </li>
            </ul>
            <div id="singleTaskList">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Kunde</th>
                            <th scope="col">Datum</th>
                            <th scope="col">Beschreibung</th>
                            <th scope="col">Einzelpreis</th>
                            <th scope="col">Anzahl</th>
                            <th scope="col">MwSt.</th>
                            <th scope="col">Brutto</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                    {tasks.map((task) => (
                        <>
                        {task.taskType === "SINGLE" ? (
                            <>
                                {customers.map((customer) => (
                                    <>
                                        {customer._id === task.customerID ? (
                                                <tr>
                                                    {customer.customerType === "PRIVATE" ? (
                                                        <td>{customer.name}</td>
                                                    ) : (
                                                        <td>{customer.companyName} {customer.legalForm}</td>
                                                    )}
                                                    <td>{moment(task.executionDate).format('DD.MM.YYYY')}</td>
                                                    <td>{task.description}</td>
                                                    <td>{task.singlePrice.toFixed(2)}</td>
                                                    <td>{task.quantity}</td>
                                                    <td>{task.vat}</td>
                                                    <td>{task.total}</td>
                                                    <td>
                                                        <button type="button" className="btn btn-primary mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#editTaskModal" onClick={() => updateTask(task._id)}>Update</button>
                                                        <button type="button" className="btn btn-danger mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#deleteTaskModal" onClick={() => setToDeleteTask(task._id)}>Löschen</button>   
                                                    </td>
                                                </tr>
                                        ) : (
                                            ""
                                        )}
                                    </>
                                    
                                ))}
                            </>
                        ) : (
                            <tr></tr>
                        )}
                        </>
                    ))}
                    </tbody>
                </table>
            </div>
            <div id="timedTaskList" className="d-none">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Kunde</th>
                            <th scope="col">Datum</th>
                            <th scope="col">Beschreibung</th>
                            <th scope="col">Stundensatz</th>
                            <th scope="col">Startzeit</th>
                            <th scope="col">Endzeit</th>
                            <th scope="col">Dauer</th>
                            <th scope="col">MwSt.</th>
                            <th scope="col">Brutto</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                    {tasks.map((task) => (
                        <>
                        {task.taskType === "TIMED" ? (
                            <>
                                {customers.map((customer) => (
                                    <>
                                        {customer._id === task.customerID ? (
                                                <tr>
                                                    {customer.customerType === "PRIVATE" ? (
                                                        <td>{customer.name}</td>
                                                    ) : (
                                                        <td>{customer.companyName} {customer.legalForm}</td>
                                                    )}
                                                    <td>{moment(task.executionDate).format('DD.MM.YYYY')}</td>
                                                    <td>{task.description}</td>
                                                    <td>{task.singlePrice.toFixed(2)}</td>
                                                    <td>{task.timeStart}</td>
                                                    <td>{task.timeEnd}</td>
                                                    <td>{timeDiff(task.timeStart, task.timeEnd)}</td>
                                                    <td>{task.vat}</td>
                                                    <td>{task.total}</td>
                                                    <td>
                                                        <button type="button" className="btn btn-primary mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#editTaskModal" onClick={() => updateTask(task._id)}>Update</button>
                                                        <button type="button" className="btn btn-danger mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#deleteTaskModal" onClick={() => setToDeleteTask(task._id)}>Löschen</button>   
                                                    </td>
                                                </tr>
                                        ) : (
                                            ""
                                        )}
                                    </>
                                    
                                ))}
                            </>
                        ) : (
                            <tr></tr>
                        )}
                        </>
                    ))}
                    </tbody>
                </table>
            </div>
            <div id="serviceTaskList" className="d-none">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Kunde</th>
                            <th scope="col">Beschreibung</th>
                            <th scope="col">Preis</th>
                            <th scope="col">Vertrag Start</th>
                            <th scope="col">Vertrag Ende</th>
                            <th scope="col">MwSt.</th>
                            <th scope="col">Brutto</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                    {tasks.map((task) => (
                        <>
                        {task.taskType === "CONTRACT" ? (
                            <>
                                {customers.map((customer) => (
                                    <>
                                        {customer._id === task.customerID ? (
                                                <tr>
                                                    {customer.customerType === "PRIVATE" ? (
                                                        <td>{customer.name}</td>
                                                    ) : (
                                                        <td>{customer.companyName} {customer.legalForm}</td>
                                                    )}
                                                    <td>{task.description}</td>
                                                    <td>{task.singlePrice.toFixed(2)}</td>
                                                    <td>{moment(task.contractStart).format('DD.MM.YYYY')}</td>
                                                    <td>{moment(task.contractEnd).format('DD.MM.YYYY')}</td>
                                                    <td>{task.vat}</td>
                                                    <td>{task.total}</td>
                                                    <td>
                                                        <button type="button" className="btn btn-primary mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#editTaskModal" onClick={() => updateTask(task._id)}>Update</button>
                                                        <button type="button" className="btn btn-danger mb-3 mx-1" data-bs-toggle="modal" data-bs-target="#deleteTaskModal" onClick={() => setToDeleteTask(task._id)}>Löschen</button>   
                                                    </td>
                                                </tr>
                                        ) : (
                                            ""
                                        )}
                                    </>
                                    
                                ))}
                            </>
                        ) : (
                            <tr></tr>
                        )}
                        </>
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Tasks