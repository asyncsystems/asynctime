import React, { useState } from "react";
import moment from "moment";
import axios from 'axios';

const Bills = () => {

    const [bills, setBills] = useState([])
    const [customers, setCustomers] = useState([])
    const [checkedTasks, setCheckedTasks] = useState([])

    React.useEffect(() => {
        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/bills",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setBills(data.data)
        }).catch(err => console.error(err));

        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/customers",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setCustomers(data.data)
        }).catch(err => console.error(err));
    }, [])

    function onResetSearchForm() {
        axios({
            "method": "GET",
            "url": "http://127.0.0.1:52445/bills",
            "headers": {
                "Content-Type": "application/json"
            }
        }).then((data) => {
            setBills(data.data)
        }).catch(err => console.error(err));
    }

    function onSubmitSearchForm() {
        var config = {
            headers: {
                'Content-Type': 'text/plain'
            }
        };
        axios.post('/bills/unbooked', document.getElementById("Kunde").value, config)
        .then((data) => {
            setBills(data.data)
        })
        .catch((err) => console.error(err));
    }

    const onSubmitGenerateBill = async () => {
        let data = document.getElementsByName("checkedTasks")
        for(const val of data) {
            const res = await fetchTask(val.value)
            let jsonarray  = JSON.parse("[" + JSON.stringify(res) + "]")

            if(val.checked == true) {
                if(!checkedTasks.find(t => t._id === jsonarray[0]["data"]["_id"])) {
                    checkedTasks.push(jsonarray[0]["data"])
                }
            } else {
                // TODO: Removing items from array not working flawless
                if(checkedTasks.find(t => t._id === jsonarray[0]["data"]["_id"])) {
                    checkedTasks.splice(checkedTasks.indexOf(jsonarray[0]["data"]), 1)
                }
            }
        }
    }

    const fetchTask = x => {
        return new Promise((resolve,reject) => {
            axios({
                "method": "GET",
                "url": "http://127.0.0.1:52445/tasks/"+x,
                "headers": {
                    "Content-Type": "application/json"
                }
            }).then((data) => {
                resolve(data);
            }).catch((err) => console.error(err));
        })
    }

    async function generateBill() {
        var config = {
            headers: {
                'Content-Type': 'text/plain'
            }
        };
        if(checkedTasks.length == 0) {
            return
        }

        let result = await axios.post('/bills/generate', JSON.stringify(checkedTasks), config);

        window.open('/bills/download/' + result.data["filename"], '_blank');

        /*fetch('/bills/generate', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            }),
            body: "task="+JSON.stringify(checkedTasks)
        }).then(data => {
            console.log(data.text);
        });*/
    }

    return (
        <div className="container">
            <center><h1>Unverbuchte Aufträge</h1></center>

            <div className="card mb-3">
                    <div className="card-body">
                        <h3 className="card-title">Einträge filtern</h3>
                        <div id="filterUnbookedTasks">
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <select className="form-select" id="Kunde" name="customerID">
                                            {customers.map((customer) => (
                                                <>
                                                    {customer.customerType === "PRIVATE" ? (
                                                        <option value={customer._id}>{customer.name}</option>
                                                    ) : (
                                                        <option value={customer._id}>{customer.companyName} {customer.legalForm}</option>
                                                    )}
                                                </>
                                            ))}
                                        </select>
                                        <label htmlFor="Kunde">Kunde</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer">
                        <button type="button" onClick={() => onSubmitSearchForm()} className="btn btn-primary mx-2">Suchen</button>
                        <button type="reset" onClick={() => onResetSearchForm()} className="btn btn-secondary mx-2">Zurücksetzen</button>
                    </div>
            </div>

            <div className="card mb-3">
                <div className="card-body">
                    <h3 className="card-title">Nicht verbuchte Aufträge</h3>
                    <div id="unBookedTaskList">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Datum</th>
                                    <th scope="col">Beschreibung</th>
                                    <th scope="col">Gesamtpreis</th>
                                </tr>
                            </thead>
                            <tbody>
                                {bills.map((bill) => (
                                    <>
                                    {bill.taskType === "CONTRACT" ? (
                                        <tr>
                                            <td><input type="checkbox" name="checkedTasks" value={bill._id} /></td>
                                            <td>{moment(bill.contractStart).format('DD.MM.YYYY')} - {moment(bill.contractEnd).format('DD.MM.YYYY')}</td>
                                            <td>{bill.description}</td>
                                            <td>{bill.total}</td>
                                        </tr>
                                    ) : (
                                        <tr>
                                            <td><input type="checkbox" name="checkedTasks" value={bill._id} /></td>
                                            <td>{moment(bill.executionDate).format('DD.MM.YYYY')}</td>
                                            <td>{bill.description}</td>
                                            <td>{bill.total}</td>
                                        </tr>
                                    )}
                                    </>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer">
                    <button type="button" onClick={() => onSubmitGenerateBill().then(() => { generateBill() })} className="btn btn-primary mx-2">Rechnung generieren</button>
                </div>
            </div>

        </div>
    )
}

export default Bills