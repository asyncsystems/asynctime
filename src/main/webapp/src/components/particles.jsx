import React, { useState } from 'react'

const Particles = () => {

    const [particles, setParticles] = useState([])

    React.useEffect(() => {
        fetch('/particles')
        .then(res => res.json())
        .then((data) => {
          setParticles(data)
        })
        .catch(console.log)
      }, [])

    return (
        <div>
            <center><h1>Particles List</h1></center>
            <table className="pf-c-table pf-m-grid-md" role="grid" aria-label="Supersonic Subatomic Particles" id="table-basic">
            <caption>Supersonic Subatomic Particles</caption>
            <thead>
                <tr role="row">
                    <th role="columnheader" scope="col">Name</th>
                </tr>
            </thead>
            {particles.map((particle) => (
                <tbody key={particle.name}>
                    <tr role="row">
                        <td role="cell" data-label="Particle name">{particle.name}</td>
                    </tr>
                </tbody>
            ))}
            </table>
            <form action="/particles" method="post">
                <button type="submit">QR-Rechnung generieren</button>
            </form>
        </div>
    )
}

export default Particles