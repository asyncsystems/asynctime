![AsyncSystems Logo](src/main/resources/img/asyncsystems.svg)

# AsyncTime Project
**By AsyncSystems**

## About the project
The application is a fully fledged time, customer and bill management-system.

It handles all the customers with it's tasks, and put them all in a bill, which can then be sent via mail or e-mail to the customer.

The software includes a quarkus-backend written in Kotlin and a ReactJS frontend, all compiled into one .jar file, which will then be executed inside an openjdk docker-container.
