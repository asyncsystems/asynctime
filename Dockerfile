FROM openjdk:18.0

RUN microdnf install maven rsync -y
RUN mkdir -p /tmp/build
WORKDIR /tmp/build
COPY . .

RUN mvn clean package

RUN mkdir -p /opt/srv/quarkus
RUN mv target/ /opt/srv/quarkus/target
WORKDIR /opt/srv/quarkus
